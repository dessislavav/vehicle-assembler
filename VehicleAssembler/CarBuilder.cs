﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VehicleAssembler
{
    public class CarBuilder : IVehicleBuilder
    {
        private readonly Vehicle objVehicle = new Vehicle();
       
        public void SetCoupe()
        {
            objVehicle.Coupe = "Sedan";
        }

        public void SetDoors()
        {
            objVehicle.Doors = 5;
        }

        public void SetEngineType()
        {
            objVehicle.EngineType = "Diesel";
        }

        public void SetEngineVolume()
        {
            objVehicle.EngineVolume = 3000;
        }

        public void SetVehicleType()
        {
            objVehicle.VehicleType = "Car";
        }

        public void SetWheels()
        {
            objVehicle.Wheels = 4;
        }

        public void SetYear()
        {
            objVehicle.Year = 2015;
        }

        public Vehicle GetVehicle()
        {
            return objVehicle;
        }
    }
}
