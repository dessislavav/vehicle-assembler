﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VehicleAssembler
{
    public class VehicleCreator
    {
        private readonly IVehicleBuilder objBuilder;

        public VehicleCreator(IVehicleBuilder builder)
        {
            objBuilder = builder;
        }

        public void CreateVehicle()
        {
            objBuilder.SetVehicleType();
            objBuilder.SetEngineType();
            objBuilder.SetCoupe();
            objBuilder.SetEngineVolume();
            objBuilder.SetDoors();
            objBuilder.SetWheels();
            objBuilder.SetYear();
        }

        public Vehicle GetVehicle()
        {
            return objBuilder.GetVehicle();
        }
    }
}
