﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VehicleAssembler
{
    public class Vehicle
    {
        public string VehicleType { get; set; }
        public string EngineType { get; set; }
        public string Coupe { get; set; }
        public int EngineVolume { get; set; }
        public int Doors { get; set; }
        public int Wheels { get; set; }
        public int Year { get; set; }

        public void ShowInfo()
        {
            Console.WriteLine("Vehicle Type: {0}", VehicleType);
            Console.WriteLine("Engine Type: {0}", EngineType);
            Console.WriteLine("Coupe: {0}", Coupe);
            Console.WriteLine("Engine Volume: {0}", EngineVolume);
            Console.WriteLine("Doors: {0}", Doors);
            Console.WriteLine("Wheels: {0}", Wheels);
            Console.WriteLine("Year: {0}", Year);
        }
    }
}
