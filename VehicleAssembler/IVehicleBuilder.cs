﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VehicleAssembler
{
    public interface IVehicleBuilder
    {
        void SetVehicleType();
        void SetEngineType();
        void SetCoupe();
        void SetEngineVolume();
        void SetDoors();
        void SetWheels();
        void SetYear();
        Vehicle GetVehicle();
    }
}
